﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VictoryScript : MonoBehaviour
{
    public RawImage image;
    public VideoPlayer video;
    public AudioSource audio;
    public float timer = 1f;

    // Start is called before the first frame update
    void Start()
    {

        StartCoroutine(PlayVideo());

    }
    IEnumerator PlayVideo()
    {
        video.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(timer);
        while (!video.isPrepared)
        {
            yield return waitForSeconds;
            break;
        }
        image.texture = video.texture;
        video.Play();
        audio.Play();
    }

    // Update is called once per frame
    void Update()
    {

    }
}

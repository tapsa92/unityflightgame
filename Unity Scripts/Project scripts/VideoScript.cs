﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class VideoScript : MonoBehaviour
{
    public RawImage image;
    public VideoPlayer video;
   
    

    // Start is called before the first frame update
    void Start()
    {
        
        StartCoroutine(PlayVideo());
        
    }
    IEnumerator PlayVideo()
    {
        
        video.Prepare();
        WaitForSeconds waitForSeconds = new WaitForSeconds(1f);
        while (!video.isPrepared)
        {
            
            yield return waitForSeconds;
            break;
        }
        image.texture = video.texture;
        video.Play();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

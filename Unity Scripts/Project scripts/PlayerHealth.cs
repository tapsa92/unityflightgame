﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

/*
 * 
 * Collisions damage
 * 
 */
public class PlayerHealth : MonoBehaviour
{
    public float regenPerSecond = 30f;
    public float collisionDamage = 0.7f;
    public Slider healthSlider;
    private float playerHp = 50f;
    private bool playerDead = false;
    private bool regenActive = true;
    public ParticleSystem SmokeDamage;
    public ParticleSystem Bomb;
    public ParticleSystem secondBomb;
    private IEnumerator endTimer;
    public AudioSource ExplosionSound;
    Rigidbody planeRigid;
    public AudioSource ImpactSound;
    public AudioSource backgroundMusic;
   
    void Start()
    {
        healthSlider.value = playerHp;
        planeRigid = GetComponent<Rigidbody>();
         Bomb.Stop();
        secondBomb.Stop();
        Bomb.gameObject.SetActive(true);
        secondBomb.gameObject.SetActive(true);
       
        // rend = GetComponentInChildren<Renderer>().material;
        //ParticleSystem.MainModule sm = SmokeDamage.main;

        // SmokeDamage.Stop();

        //sm.startColor = SmokeColor.Evaluate(Random.Range(0f, 1f));
        //SmokeDamage.emission.rate = new ParticleSystem.MinMaxCurve(5.0f);
        // SmokeColor = GetComponentInChildren<Renderer>().material;
        ImpactSound.Stop();
        
    }
    void Update()
    {
        
         if(playerHp == 50f)
        {
            SmokeDamage.Stop();
            Bomb.Stop();
            secondBomb.Stop();
            ExplosionSound.Stop();
        }
         else
        {
            SmokeDamage.Play();
            smokeAmount();
        }
       
    }
    private void smokeAmount()
    {
        
        if (playerHp < 35f && playerHp > 20f)
        {
            SmokeDamage.Emit(1);
            //rend.material.color = whiteColor;

        }
        if (playerHp < 20f && playerHp > 5f)
        {
            SmokeDamage.Emit(2);
            
        }
        if (playerHp < 5f && playerHp > 0f)
        {
            SmokeDamage.Emit(4);
 
        }
        if (playerHp == 0f)
        {
            PlayerDeath();
        }

    }

    

    private void OnCollisionStay()
    {
        // TODO: smoke etc from airplane

        DamagePlayer(collisionDamage);
        
        
    }

    // Positive values for health loss, negative values for health regen
    public void DamagePlayer(float dmgTaken)
    {
        
        regenActive = false;
        
        playerHp -= dmgTaken;
        if (playerHp < 0) playerHp = 0;

        healthSlider.value = playerHp;
        if (playerHp == 0 && !playerDead)
        {
            PlayerDeath();
        }
        
    }

    public void HealPlayer(float dmgHealed)
    {
        playerHp += dmgHealed;
        if (playerHp > 50f) playerHp = 50f;       
        healthSlider.value = playerHp;
    }

    private IEnumerator HealthRegeneration(float regenPerSecond)
    {
        if (!regenActive)
        {
            ImpactSound.Play();
            regenActive = true;
            yield return new WaitForSeconds(3f);
            while (playerHp < 50f && regenActive)
            {
                HealPlayer(regenPerSecond / 50);
                yield return new WaitForSeconds(0.01f);
            }
        }
    }
    private void OnCollisionExit()
    {
        StartCoroutine(HealthRegeneration(regenPerSecond));
    }

    private void PlayerDeath()
    {
        playerDead = true;
        backgroundMusic.Stop();
        
        //SmokeDamage.Stop();
        endTimer = GameOverTime(5f);
        ExplosionSound.Play();
        StartCoroutine(endTimer);       
        Bomb.Play();
        secondBomb.Play();

        // planeRigid.transform.Rotate(0.5f, 0, 1.5f);
        planeRigid.freezeRotation = false;
        gameObject.GetComponent<PlaneControls>().enabled = false;
        planeRigid.useGravity = true;
        //Destroy(Bomb, Bomb.duration);
        

        // camera stop?
        // explosion trigger?
        // after animation go to game over scene 
    }

    IEnumerator GameOverTime(float waitTime)
    {
        
        yield return new WaitForSeconds(waitTime);      
        SceneManager.LoadScene("GameOver");
    }
}

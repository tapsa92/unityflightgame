﻿
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
    
    static TextMeshProUGUI scoreValue;
    private float timer = 0f;
    private int minutes = 0;
    TextMeshProUGUI timerValue;
    // Start is called before the first frame update
    void Start()
    {
        timerValue = transform.FindChild("TimeText").GetComponent<TextMeshProUGUI>();
        scoreValue = transform.FindChild("ScoreText").GetComponent<TextMeshProUGUI>();

    }

    // Update is called once per frame
    void Update()
    {
        // Updating timer value...
        timer += Time.deltaTime;
        if (timer > 60f)
        {
            timer -= 60f;
            minutes++;
        }
        string timerString = minutes + ":";
        if (timer < 10) timerString += 0;
        timerString += timer.ToString("F2");
        timerValue.text = "Time: " + timerString;
        StaticVariables.PlayerCompletionTime = timerString;
       
    }

    // Updates UI with +1 coin collected
    // Called from Coin's script
    public static void updateCoins()
    {
        StaticVariables.PlayerCollectedCoins++;
        scoreValue.text = "Score: " + StaticVariables.PlayerCollectedCoins;
      
        
        
    }




}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class VictoryValuesScript : MonoBehaviour
{
    static TextMeshProUGUI score;
    static TextMeshProUGUI time;
    // Start is called before the first frame update
    void Start()
    {
        time = transform.FindChild("Time").GetComponent<TextMeshProUGUI>();
        score = transform.FindChild("CoinsCollected").GetComponent<TextMeshProUGUI>();
        score.text = "Collected coins: "+ StaticVariables.PlayerCollectedCoins;
        time.text = "Time: " + StaticVariables.PlayerCompletionTime;
    }

    // Update is called once per frame
    void Update()
    {
        
    }


}

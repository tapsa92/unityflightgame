﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;



/*
 * 
 * APPLY TO A PARENT OBJECT CONTAINING ALL CHECKPOINTS AS CHILDREN
 * 
 */
public class RaceLogic : MonoBehaviour
{
    public int childrenAmount = 0;
    public static Transform[] checkpointArray = new Transform[20];
    public static int currentCheckpoint = 0;    // Static so the same value can be accessed by all checkpoint objects
    

    void Start()
    {
        currentCheckpoint = 0;
        childrenAmount = transform.childCount;
        for (int i = 0; i < childrenAmount; i++)
        {
            checkpointArray[i] = transform.GetChild(i);
        }
        StaticVariables.PlayerCollectedCoins = 0;
        for (int i = 0; i < childrenAmount; i++)
        {
            checkpointArray[i].GetComponent<CheckpointScript>().GrandChildSetActive(false);
        }
        checkpointArray[currentCheckpoint].GetComponent<CheckpointScript>().GrandChildSetActive(true);

    }

    void Update()
    {
        //Debug.Log(currentCheckpoint);
    }

    public void checkpointReached()
    {
        currentCheckpoint++;
        for (int i = 0; i < childrenAmount; i++)
        {
            checkpointArray[i].GetComponent<CheckpointScript>().GrandChildSetActive(false);
        }
       
        Debug.Log("Checkpoint " + currentCheckpoint + " reached!");
        if (currentCheckpoint == transform.childCount)
        {
            currentCheckpoint = 0;  // Set first checkpoint active again..
            // Code for player victory here
            StartCoroutine(GoToVictory());

        }
         checkpointArray[currentCheckpoint].GetComponent<CheckpointScript>().GrandChildSetActive(true);
    }
    IEnumerator GoToVictory()
    {

        WaitForSecondsRealtime waitForSecondsRealtime = new WaitForSecondsRealtime(1f);


        yield return waitForSecondsRealtime;
        SceneManager.LoadScene("VictoryScreen");
    }
}


